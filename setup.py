#!/usr/bin/env python

from setuptools import setup

# The version is updated automatically with bumpversion
# Do not update manually
__version = '1.0.2'


def main():

    setup(
        name='tangoepics',
        version=__version,
        package_dir={'tangoepics': 'tangoepics'},
        packages=['tangoepics'],
        include_package_data=True,  # include files in MANIFEST
        author='Jairo Moldes',
        author_email='jmoldes@cells.es',
        description='Tango device server for accessing Epics PVs',
        license='GPLv3+',
        platforms=['src', 'noarch'],
        url='https://gitlab.com/tango-controls/device-servers/DeviceClasses'
            '/communication/TangoEpics.git',
        requires=['tango (>=7.2.6)'],
        entry_points={
            'console_scripts': [
                'TangoEpics = tangoepics.TangoEpics:main',
            ],
        },
    )


if __name__ == "__main__":
    main()
